import requests, base64, sys

def deploy_file(name, file_path):
    payload = { "file": { "name": name, "content":base64.b64encode(get_file_data(file_path))}}
    r = requests.put("https://0.0.0.0:5554/mgmt/filestore/default/local/test-directory/{file_name}".format(file_name=name), auth=("admin", "admin"), data=payload)

def get_file_data(file_path):
    data = ''
    with open (file_path, "r+") as file:
        data = file.read()
    return data
    
print("hello")